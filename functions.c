#include "functions.h"
#define N 5 //constante représentant le nombre lignes


image loadImg(char* nom){


  image img;
  img.type = malloc(sizeof(char)*N);


  FILE* fichier;
  char ligne[N];
  int nbLignesLues = 0;
  int choixpx = 0;
  int nbpxligne = 0;
  int nbpxcol = 0;

  fichier = fopen(nom, "r");


  while(fgets(ligne,N,fichier) !=NULL){
    if(nbLignesLues == 0){
      for (int i = 0; i < N; i++) {
        img.type[i] = ligne[i];
      }
      nbLignesLues++;
    }else if(nbLignesLues==1){
      img.largeur = atoi(ligne);
      nbLignesLues++;
    }else if(nbLignesLues==2){
      img.hauteur = atoi(ligne);
      nbLignesLues++;
    }else if(nbLignesLues==3){
      img.px = malloc(sizeof(pixel*)*img.largeur);
      for (int i = 0; i < img.hauteur; i++) {
        img.px[i] = malloc(sizeof(pixel)*img.hauteur);
      }
      img.valmax = atoi(ligne);
      nbLignesLues++;
    }else{
      switch (choixpx) {
        case 0:
          img.px[nbpxligne][nbpxcol].r = atoi(ligne);
          choixpx = 1;
          nbLignesLues++;
          break;
        case 1:
          img.px[nbpxligne][nbpxcol].v = atoi(ligne);
          choixpx = 2;
          nbLignesLues++;
          break;
        case 2:
          img.px[nbpxligne][nbpxcol].b = atoi(ligne);
          choixpx = 0;
          nbLignesLues++;
          nbpxcol++;
          if(nbpxcol == img.largeur){
            nbpxligne++;
            nbpxcol = 0;
          }
          break;
        default:
          choixpx = 0;
          nbLignesLues = 0;
          printf("Erreur lors de l'enregistrement des pixels");
          break;
      }
    }
  }

      fclose(fichier);
      return img;
}



int saveImg(image img, char* nom){

  FILE* fichier;

  fichier = fopen(nom, "w");

  if(fichier == NULL){
    fclose(fichier);
    return 0;
  }else{
    fprintf(fichier, "%s", img.type);
    fprintf(fichier, "%d %d\n", img.largeur, img.hauteur);
    fprintf(fichier, "%d\n", img.valmax);

    for (int i = 0; i < img.largeur; i++) {
      for (int j = 0; j < img.hauteur; j++) {
        fprintf(fichier, "%d\n", img.px[i][j].r);
        fprintf(fichier, "%d\n", img.px[i][j].v);
        fprintf(fichier, "%d\n", img.px[i][j].b);

      }
    }

    fclose(fichier);
    return 1;
  }

}



void makeImg( int x, int y, int epaisseur,char* nom){

}


void niveau2Gris(image img, char* nom){

  int gris;

    //on parcours l'image
    for (int i = 0; i < img.hauteur; i++) {
      for (int j = 0; j < img.largeur; j++) {
        //on calcule la nouvelle valeur de gris
        gris = (0.2126*img.px[i][j].r) + (0.7152*img.px[i][j].v) + (0.0722*img.px[i][j].b);

        //chaque composant du pixel prend la nouvelle valeur "gris"
        img.px[i][j].r = gris;
        img.px[i][j].v = gris;
        img.px[i][j].b = gris;
      }
    }

    //sauvegarde de l'image dans un fichier
    saveImg(img,nom);
}


void negatifImg(image img, char* nom){
  int nr,nv,nb;

  // on parcours l'image dans sa globalité
  for (int i = 0; i <img.hauteur ; i++) {
      for (int j = 0; j < img.largeur; j++) {

        // on initialise dans une variable temporaire les valeurs des composante des pixels.
        nr = img.px[i][j].r ;
        nv = img.px[i][j].v ;
        nb = img.px[i][j].b ;

        // le nouveau pixel prend alors la valeur maximale
        img.px[i][j].r= 255-nr;
        img.px[i][j].v= 255-nv;
        img.px[i][j].b=255-nb;
      }
  }

  //sauvegarde de l'image dans un fichier
  saveImg(img,nom);
}


void binaire (image img, int seuil, char* nom){

    //on parcours l'image dans sa globalité
    for (int i = 0; i < img.hauteur; i++){
        for (int j = 0; j < img.largeur; j++){

            //cas ou la moyenne des composantes du pixel est inférieur au seuil
            if (((img.px[i][j].r+img.px[i][j].v+img.px[i][j].b)/3) < seuil)
            {
                // si oui, on initialise le pixel à la valeur d'un pixel noir
                img.px[i][j].r = 0;
                img.px[i][j].v = 0;
                img.px[i][j].b = 0;
            }else{
                // sinon initialise le pixel à la valeur d'un pixel blanc
                img.px[i][j].r = 255;
                img.px[i][j].v = 255;
                img.px[i][j].b = 255;
            }
        }
    }

    //sauvegarde de l'image dans un fichier
    saveImg(img,nom);
}

void rotation(image img, char* nom){

  // on créer une nouvelle image représentant la rotation
  image rotaImg;

  // on alloue le type de la nouvelle image
  rotaImg.type = malloc(sizeof(char)*N);

  //l'image de rotation prend les valeurs de l'image de base (taille/type/valmax)
  rotaImg.type = img.type;
  rotaImg.largeur = img.hauteur;
  rotaImg.hauteur = img.largeur;
  rotaImg.valmax = img.valmax;


  //on alloue le nombre de pixels de la nouvelle en fonction de sa taille
  rotaImg.px = malloc(sizeof(pixel)*img.largeur);
  for (int k = 0; k < img.hauteur; k++) {
    rotaImg.px[k] = malloc(sizeof(pixel)*img.hauteur);
  }

  //parcours de l'image de rotation
  for (int i = 0; i < img.hauteur; i++){
        for (int j = 0; j < img.largeur; j++){

          //opération permettant le passage a 90° de l'image de base
          rotaImg.px[j][(img.largeur-1)-i]=img.px[i][j];
        }
    }

  //sauvegarde de l'image dans un fichier
  saveImg(rotaImg,nom);

  //libération de l'espace utilsier par l'image de rotation
  free(rotaImg.type);
  for (int k = 0; k < img.hauteur; k++) {
    free(rotaImg.px[k]);
  }
  free(rotaImg.px);

}

void miroir(image img, char* nom){

  //création de la nouvelle image représentant l'image miroir
  image mirImg;

  //allocation du type de cette image
  mirImg.type = malloc(sizeof(char)*N);

  //l'image miroir prend les valeurs de l'image de base (taille/type/valmax)
  mirImg.type = img.type;
  mirImg.hauteur = img.hauteur;
  mirImg.largeur = img.largeur;
  mirImg.valmax = img.valmax;

  //on alloue l'espace que prendra les pixels dans l'image
  mirImg.px = malloc(sizeof(pixel*)*img.largeur);
  for (int k = 0; k < img.hauteur; k++) {
    mirImg.px[k] = malloc(sizeof(pixel)*img.hauteur);
  }

  // on parcours l'image
  for (int i = 0; i < img.hauteur; i++){
    for (int j = 0; j < img.largeur; j++){
      //calcul utiliser permettant ainsi aux pixels deprendre leurs nouvelle position dans l'image miroir
      mirImg.px[i][(img.largeur-1)-j]=img.px[i][j];
    }
  }

  //sauvegarde de l'image dans un nouveau fichier
  saveImg(mirImg,nom);

  //on libère l'espace utiliser par l'image miroir
  free(mirImg.type);
  for (int k = 0; k < img.hauteur; k++) {
    free(mirImg.px[k]);
  }
  free(mirImg.px);
}



//Idée d'amélioration : Sauvegarde d'une image représentant l'histogramme
int* histogramme(image img){


  //initialisation d'un tableau représentant l'histogramme
  int* tab;

  //allocation du tableau
  tab = malloc(sizeof(int) * 256);

  //initialisation de chaque valeur du tableau a 0
  for (int i = 0; i < 256; i++) {
    tab[i] = 0;
  }

  //on parcours le tableau représentant l'histogramme
  for (int k = 0; k < img.valmax+1; k++) {

    //on parcours chaque pixels de l'image
    for (int i = 0; i < img.hauteur; i++) {
      for (int j = 0; j < img.largeur; j++) {

        //lorsque la valeur de la composante rouge du pixel est égale à la valeur de k ([0,255]) on rajoute 1 a la valeur représentantante dans tab[]
        if (k == img.px[i][j].r) {
          tab[k] = tab[k] + 1;
        }

        //lorsque la valeur de la composante verte du pixel est égale à la valeur de k ([0,255]) on rajoute 1 a la valeur représentantante dans tab[]

        if(k == img.px[i][j].v){
          tab[k] = tab[k] + 1;
        }

        //lorsque la valeur de la composante bleue du pixel est égale à la valeur de k ([0,255]) on rajoute 1 a la valeur représentantante dans tab[]

        if(k == img.px[i][j].b){
          tab[k] = tab[k] + 1;
        }
      }
    }
  }

  // on retourne le tableau représentant l'histogramme
  return tab;
}

void recadragedyn(image img, char* nom){


  int* histo;
  int maxL, minL, max, min;
  double delta;

  // on alloue la variable représentant l'histogramme
  histo = malloc(sizeof(int)*256);
  maxL = 0;
  max = 1;
  min = 99999;
  minL = 0;

  // histo prend la valeur du tableau de l'histogramme de l'image
  histo = histogramme(img);

  // on parcours l'histogramme
  for (int i = 0; i < 256; i++) {

    //si la valeur de l'histogramme est supérieur au max
    if (histo[i] >= max) {
      //max prend la valeur du nombre max du pigment de couleur
      max = histo[i];
      //maxL prend la valeur du pigment de couleur max
      maxL = i;
    }
    //si la valeur de l'histogramme est inférieure au min, min = la valeur
    else if(histo[i] < min){
      //min prend la valeur du nombre min du pigment de couleur
      min = histo[i];
      //minL prend la valeur du pigment de couleur min
      minL = i;
    }
  }

  //calcul de delta avec la formule
  delta = (double) 255 / ((double)maxL - (double)minL);


  //on parcours l'image (ses pixels)
  for (int i = 0; i < img.hauteur; i++) {
    for (int j = 0; j < img.largeur; j++) {

      //chaque valeur de r,v,b de l'image prend la valeur de r, v ,b de l'image-minL * le delta
      img.px[i][j].r =  (img.px[i][j].r - minL)*delta;
      img.px[i][j].v =  (img.px[i][j].v - minL)*delta;
      img.px[i][j].b =  (img.px[i][j].b - minL)*delta;
    }

  }

  //on sauvegarde l'image
  saveImg(img,nom);
}

void convolution(image img, double** filtre){

  //création de l'image représentant l'image de convolution
  image convo;

  int r,v,b;

  //l'image convo prend les valeurs de l'image de base (taille/type/valmax)
  convo.hauteur=img.hauteur;
  convo.largeur=img.largeur;
  convo.valmax=img.valmax;
  convo.type=img.type;


  //on parcours l'image de convo(tout ses pixels)
  convo.px=malloc(img.largeur*sizeof(pixel*));
  for (int i=0 ; i<img.largeur ; i++){
    convo.px[i]=malloc(img.hauteur*sizeof(pixel));
  }

  //parcours de l'image d'origine
  for (int i=1 ; i<img.hauteur-1 ; i++){
    for (int j=1 ; j<img.largeur-1 ; j++){

      //parcours d'une image plus grande de 2lignes et 2 colonnes
      for(int k=i-1 ; k<=i+1 ; k++){
        for (int l=j-1 ; l<=j+1 ; l++){

          //formule utilisée pour la convolution des pixels
          r = r +(filtre[k-i+1][l-j+1]*img.px[k][l].r);
          v = v +(filtre[k-i+1][l-j+1]*img.px[k][l].v);
          b = b +(filtre[k-i+1][l-j+1]*img.px[k][l].b);
        }
      }

      //si les composantes des pixels sont inférieures à 0, alors on les initialise a 0
       if (r<0){
         r=0;
       }
       if (v<0){
         v=0;
       }
       if (b<0){
         b=0;
       }

      //on assigne les nouvelles valeurs à l'image de convolution
      convo.px[i][j].r = r;
      convo.px[i][j].v = v;
      convo.px[i][j].b = b;
    }
  }
}

void renforceContrast(image img,char* nom){
  //création d'un tableau dyn de doubles représentant les filtres
  double** filtre;

  //allocation du filtre de taille 3*3
  filtre = malloc(sizeof(double*)*3);
  for (int i = 0; i < 3; i++) {
    filtre[i] = malloc(sizeof(double)*3);
  }

  //implémentation des valeurs dans la matrice filtre
  filtre[0][0] = 0;
  filtre[0][1] = -1;
  filtre[0][2] = 0;
  filtre[1][0] = -1;
  filtre[1][1] = 5;
  filtre[1][2] = -1;
  filtre[2][0] = 0;
  filtre[2][1] = -1;
  filtre[2][2] = 0;

  //utilisation de la convolution
  convolution(img, filtre);

  //sauvegarde de l'image
  saveImg(img, nom);
}

void floutage(image img, char* nom){
  //création d'un tableau dyn de doubles représentant les filtres
  double** filtre;

  //allocation du filtre de taille 3*3
  filtre = malloc(sizeof(int*)*3);
  for (int i = 0; i < 3; i++) {
    filtre[i] = malloc(sizeof(int)*3);
  }

  //implémentation des valeurs dans la matrice filtre
  filtre[0][0] = 0.0625;
  filtre[0][1] = 0.125;
  filtre[0][2] = 0.0625;
  filtre[1][0] = 0.125;
  filtre[1][1] = 0.25;
  filtre[1][2] = 0.125;
  filtre[2][0] = 0.0625;
  filtre[2][1] = 0.125;
  filtre[2][2] = 0.0625;

  //utilisation de la convolution avec le filtre
  convolution(img, filtre);

  //sauvegarde de l'image
  saveImg(img, nom);
}


void contour(image img, char*nom){

  //création d'un tableau dyn de doubles représentant les filtres
  double** filtre;

  //allocation du filtre de taille 3*3
  filtre = malloc(sizeof(int*)*3);
  for (int i = 0; i < 3; i++) {
    filtre[i] = malloc(sizeof(int)*3);
  }

  //implémentation des valeurs dans la matrice filtre
  filtre[0][0] = -1;
  filtre[0][1] = -1;
  filtre[0][2] = -1;
  filtre[1][0] = -1;
  filtre[1][1] =  8;
  filtre[1][2] = -1;
  filtre[2][0] = -1;
  filtre[2][1] = -1;
  filtre[2][2] = -1;

  //utilisation de la convolution avec le filtre
  convolution(img, filtre);

  //sauvegarde de l'image
  saveImg(img, nom);
}



void erosion(image img, croix crx, char* nom){


  //Créer une matrice avec 2 espaces de +
  //mettre l'image dans la matrice
  //donner des valeurs a la croix
  //analyser les valeurs de la croix et trouver le minimum
  //remplacer les valeurs de l'image par le min
  int min;

  crx.px = malloc(sizeof(pixel*)*5);
  for (int i = 0; i < 5; i++) {
    crx.px[i] = malloc(sizeof(pixel)*5);
  }



    image pxt;

    pxt.type = malloc(sizeof(char)*10);
    pxt.hauteur=img.hauteur;
    pxt.largeur=img.largeur;
    pxt.valmax=img.valmax;
    pxt.type=img.type;



    pxt.px = malloc(sizeof(pixel*)*img.hauteur+2);
    for (int i = 0; i < img.hauteur+2; i++){
      pxt.px[i] = malloc(sizeof(pixel)*img.hauteur+2);
    }



    for (int i=0 ; i<img.hauteur+1 ; i++){
      for (int j=0 ; j<img.largeur+1 ; j++){
        pxt.px[i][j].r = 255;
        pxt.px[i][j].v = 255;
        pxt.px[i][j].b = 255;
      }
    }

    for (int i=2 ; i<img.hauteur-2 ; i++){
      for (int j=2 ; j<img.largeur-2 ; j++){

        for(int k=i-2 ; k<=i+2 ; k++){
          for (int l=j-2 ; l<=j+2 ; l++){
            pxt.px[i][j] = img.px[k][l];


          }
        }
      }
    }



    for(int k=2 ; k<img.hauteur-2 ; k++){
      for (int l=2 ; l<img.largeur-2 ; l++){
        min = 255;

        crx.px[0][2] = pxt.px[k-2][l];
        if(crx.px[0][2].r < min){
          min =pxt.px[k-2][l].r;
        }

        crx.px[1][2] = pxt.px[k-1][l];
        if(crx.px[1][2].r < min){
          min =pxt.px[k-1][l].r;
        }
        crx.px[3][2] = pxt.px[k+1][l];
        if(crx.px[3][2].r < min){
          min =pxt.px[k+1][l].r;
        }
        crx.px[4][2] = pxt.px[k+2][l];
        if(crx.px[4][2].r < min){
          min =pxt.px[k+2][l].r;
        }

        crx.px[2][2] = pxt.px[k][l];
        if(crx.px[2][2].r < min){
          min =pxt.px[k][l].r;
        }

        crx.px[2][4] = pxt.px[k+2][l];
        if(crx.px[2][4].r < min){
          min =pxt.px[k+2][l].r;
        }
        crx.px[2][0] = pxt.px[k-2][l];
        if(crx.px[2][0].r < min){
          min =pxt.px[k-2][l].r;
        }
        crx.px[2][1] = pxt.px[k-1][l];
        if(crx.px[2][1].r < min){
          min =pxt.px[k-1][l].r;
        }
        crx.px[2][3] = pxt.px[k+1][l];
        if(crx.px[2][3].r < min){
          min =pxt.px[k+1][l].r;
        }

        for(int i=k-2 ; i<=k+2 ; i++){
          for (int j=l-2 ; j<=l+2 ; j++){
            img.px[i][j].r = min;
            img.px[i][j].v = min;
            img.px[i][j].b = min;
          }
        }

      }
    }

  saveImg(img, nom);

}


void dilatation(image img, croix crx, char* nom){


  //Créer une matrice avec 2 espaces de +
  //mettre l'image dans la matrice
  //donner des valeurs a la croix
  //analyser les valeurs de la croix et trouver le minimum
  //remplacer les valeurs de l'image par le min
  int max;

  crx.px = malloc(sizeof(pixel*)*5);
  for (int i = 0; i < 5; i++) {
    crx.px[i] = malloc(sizeof(pixel)*5);
  }



    image pxt;

    pxt.type = malloc(sizeof(char)*10);
    pxt.hauteur=img.hauteur;
    pxt.largeur=img.largeur;
    pxt.valmax=img.valmax;
    pxt.type=img.type;



    pxt.px = malloc(sizeof(pixel*)*img.hauteur+2);
    for (int i = 0; i < img.hauteur+2; i++){
      pxt.px[i] = malloc(sizeof(pixel)*img.hauteur+2);
    }



    for (int i=0 ; i<img.hauteur+1 ; i++){
      for (int j=0 ; j<img.largeur+1 ; j++){
        pxt.px[i][j].r = 0;
        pxt.px[i][j].v = 0;
        pxt.px[i][j].b = 0;
      }
    }

    for (int i=2 ; i<img.hauteur-2 ; i++){
      for (int j=2 ; j<img.largeur-2 ; j++){

        for(int k=i-2 ; k<=i+2 ; k++){
          for (int l=j-2 ; l<=j+2 ; l++){
            pxt.px[i][j] = img.px[k][l];


          }
        }
      }
    }



    for(int k=2 ; k<img.hauteur-2 ; k++){
      for (int l=2 ; l<img.largeur-2 ; l++){
        max = 0;

        crx.px[0][2] = pxt.px[k-2][l];
        if(crx.px[0][2].r > max){
          max =pxt.px[k-2][l].r;
        }

        crx.px[1][2] = pxt.px[k-1][l];
        if(crx.px[1][2].r > max){
          max =pxt.px[k-1][l].r;
        }
        crx.px[3][2] = pxt.px[k+1][l];
        if(crx.px[3][2].r > max){
          max =pxt.px[k+1][l].r;
        }
        crx.px[4][2] = pxt.px[k+2][l];
        if(crx.px[4][2].r > max){
          max =pxt.px[k+2][l].r;
        }

        crx.px[2][2] = pxt.px[k][l];
        if(crx.px[2][2].r > max){
          max =pxt.px[k][l].r;
        }

        crx.px[2][4] = pxt.px[k+2][l];
        if(crx.px[2][4].r > max){
          max =pxt.px[k+2][l].r;
        }
        crx.px[2][0] = pxt.px[k-2][l];
        if(crx.px[2][0].r > max){
          max =pxt.px[k-2][l].r;
        }
        crx.px[2][1] = pxt.px[k-1][l];
        if(crx.px[2][1].r > max){
          max =pxt.px[k-1][l].r;
        }
        crx.px[2][3] = pxt.px[k+1][l];
        if(crx.px[2][3].r > max){
          max =pxt.px[k+1][l].r;
        }

        for(int i=k-2 ; i<=k+2 ; i++){
          for (int j=l-2 ; j<=l+2 ; j++){
            img.px[i][j].r = max;
            img.px[i][j].v = max;
            img.px[i][j].b = max;
          }
        }

      }
    }

  saveImg(img, nom);

}
