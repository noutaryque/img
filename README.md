Installation:
  git clone https://gitlab.etude.eisti.fr/noutaryque/img.git
  sudo apt-get install gnuplot  

usage : ./img [-h] [-x <largeur> <hauteur> <épaisseur>] [-i <fichier.ppm>] [-b <seuil>] [-c] [-d] [-e] [-f] [-l] [-g] [-r] [-m] [-p] [-n] [-o <fichier.ppm>]

Ci-dessous les commandes usuel pour manipuler des images PPM :

indication:
 -h : affiche une aide sur votre programme et quitte le programme avec un code de retour égal à 0 ;

Entré et Sortie:

-i <fichier.ppm> : définit le fichier d’entrée, OBLIGATOIRE (sauf si -h  ou -x utilisée) ;
-o <fichier.ppm> : définit le fichier de sortie (sauf si -h utilisée) ;

Réaliser une croix :

-x <largeur> <hauteur> <épaisseur> : réalise une croix aux dimensions spécifiées

Travailler sur la modification de l'image :

  Manipulation basiques :
    -g : convertit l’image en niveau de gris ;
    -b <seuil> : binarise une image ;
    -m : réalise le miroir d’une image ;
    -p : pivote l’image de 90° dans le sens horaire ;
    -n : réalise le négatif d’une image ;
    -r : réalise un recadrage dynamique ;

  Filtrage :
    -c : réalise un renforcement de contraste ;  
    -f : réalise un flou ;
    -l : réalise une détection de contours/lignes ;

  Segmentation :
    -d : réalise une dilatation ;
    -e : réalise une érosion ;  

ATTENTION :
  Petit problème de convolution (DERNIERE LIGNE ET DERNIERE COLONNE NON FONCTIONNELLE)
  Problème dans le recadrage dynamiquess
