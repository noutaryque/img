#ifndef __FUNCTION_H_
#define __FUNCTION_H__
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


struct pixel
{
    int r;
    int v;
    int b;
};

typedef struct pixel pixel;

struct image
{
    char* type;
    int largeur;
    int hauteur;
    int nbval;
    pixel** px;
};

typedef struct image image;

struct croix
{
  int taille;
  pixel** px;
};

typedef struct croix croix;

image loadImg(char* nom);
int saveImg(image img, char* nom);

// A continuer
void makeImg(char* nom, int x, int y, int epaisseur);


void niveau2Gris(image img);
void binaire(image img, int seuil);
void negatif(image img);
void miroir(image img);
void rotation(image img);
int* histogramme(image img);
void recadragedyn(image img);
void convolution(image img, double** filtre);
void renforceContrast(image img);
void floutage(image img);
void contour(image img);
void erosion(image img,croix croix);
#endif
