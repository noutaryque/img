imge: main.o functions.o
	gcc main.o functions.o -o imge -Wall

functions.o: functions.h functions.c
	gcc -c functions.c -o functions.o -Wall

clean:
	rm -f *.o
